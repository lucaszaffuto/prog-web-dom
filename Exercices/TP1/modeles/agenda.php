<!DOCTYPE html>
<html>
<head>
	<title>Agenda</title>
    <link href="agenda.css" rel="stylesheet">
</head>
<body>
	<h1>Agenda</h1>
    <form method="get" action="">
        <label for="mois">Month</label> <input type="mois" id="mois" <?php if(isset($_GET["mois"])) {echo 'value="'.$_GET["mois"].'"';}?>name="mois" />
        <label for="annee">Year</label> <input type="annee" id="annee" <?php if(isset($_GET["annee"])) {echo 'value="'.$_GET["annee"].'"';}?>name="annee" /><br />
        <input type="radio" id="colonne" name="radio" <?php if(isset($_GET["radio"])) {if($_GET["radio"] == "colonne") { echo "checked";}}?> value="colonne"><label for="colonne">Column</label><br>
        <input type="radio" id="tableau" name="radio" <?php if(isset($_GET["radio"])) {if($_GET["radio"] == "tableau") { echo "checked";}}?> value="tableau"><label for="tableau">Table</label><br>
        <input type="submit" value="Submit"/>
    </form>
    <?php
        
        if(!isset($_GET["mois"])) $mois = date("m");
        else  $mois = $_GET["mois"];
        if(!isset($_GET["annee"])) $annee = date("Y");
        else  $annee = $_GET["annee"];
        echo '<h2 class="date">'.$mois."/".$annee.'</h2>';
        if(isset($_GET["radio"]))
        {
            $radio = $_GET["radio"];
            if($radio == "tableau")
            {
                echo "<table>";
                echo "<tr><th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Thursday</th><th>Friday</th><th>Saturday</th><th>Sunday</th></tr>";
                
                $nb = date("t",mktime(0, 0, 0, $mois, 1, $annee));
                $jour = 1;
                $firstday = date("N", mktime(0, 0, 0, $mois, 1, $annee))-1;
                echo '<tr>';
                for($i = 0; $i <7; $i++)
                {
                    if($i <$firstday) echo '<th class = "col3"></th>';
                    else {
                        echo '<th class = "col3">'.date("d", mktime(0, 0, 0, $mois, $jour, $annee)).'</th>';
                        $jour++;
                    }
                }
                echo '</tr>';
                for($i = $jour; $i <=$nb; $i++){
                    if (date("N", mktime(0, 0, 0, $mois, $i, $annee)) == 1) echo '<tr>';
                    echo '<th class = "col3">'.date("d", mktime(0, 0, 0, $mois, $i, $annee)).'</th>';

                    if (date("N", mktime(0, 0, 0, $mois, $i, $annee)) == 7) echo '</tr>';

                }
                for($i = 0; $i <7-date("N", mktime(0, 0, 0, $mois, $nb, $annee)); $i++)
                {
                    echo '<th class = "col3"></th>';
                }
                


                
                echo "</table>";
            }
            else
            {
                echo "<table>";

                $nb = date("t",mktime(0, 0, 0, $mois, 1, $annee));
                for ($i = 1; $i <= $nb; $i++)
                {
                    echo '<tr>';
                    echo '<th class="col1">'.$i.'</th>';
                    echo '<th class="col2">'.date("l", mktime(0, 0, 0, $mois, $i, $annee)).'</th>';
                    echo '<th class="col3"></th>';
                    echo '</tr>';
                }
                echo "</table>";
            }
        }
        

    ?>
</body>
</html>