<?php

$a = 12;
$b = 123.45;
$c = 'valeur textuelle';
$d = "chaîne de $c";

$t = ['un', 'deux', 'trois'];
$u = ['one' => 'un', 'two' => 'deux', 'three' => 'trois'];

echo "\n a=";
print_r($a);
echo "\n b=";
print_r($b);
echo "\n c=";
print_r($c);
echo "\n d=";
print_r($d);

echo "\n t=";
print_r($t);
echo "\n u=";
print_r($u);

# Maintenant, remplacez les appels à var_dump() par print_r()

