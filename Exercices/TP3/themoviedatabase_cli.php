<?php
require_once("tp3-helpers.php");

$content = tmdbget("movie/".$argv[1], ['language' => 'fr']);
$json = json_decode($content);
$id = $json->{'id'};
$title = $json->{'title'};
$overview = $json->{'overview'};

echo "id=".$id." \ntitre=".$title." \ndescription=".$overview."\n";
?>