<!DOCTYPE html>
<html>
<head>
	<title>The Movie Database</title>
    <link href="lotr.css" rel="stylesheet">
</head>
<body>
    <h1>The Movie Database - Collection</h1>
    <form method="get" action="">
        <label for="id">Id</label> <input type="id" id="id" <?php if(isset($_GET["id"])) {echo 'value="'.$_GET["id"].'"';}?>name="id" />
        <input type="submit" value="Submit"/>
    </form>
    <hr>
    <?php
    require_once("tp3-helpers.php");

    
        echo '<table>';
        $id = isset($_GET['id']) ? $_GET['id'] : 119;
        $content= tmdbget("collection/".$id, ['language' => 'fr']);

        
        $json = json_decode($content);

        $parts = $json->{'parts'};
        
        $array = array();
        $acteurs_film_compt = array();
        foreach ($parts as $movie)
        {
            $id = $movie->{'id'};
            $title = '<div class="title">'.$movie->{'title'}.'</div>';
            $overview = '<p>'.$movie->{'overview'}.'</p>';
            $poster_path = $movie->{'poster_path'};
            $poster_url = "<img src= 'https://image.tmdb.org/t/p/w300".$poster_path."'>";
            $pageTMDB = 'https://www.themoviedb.org/movie/'.$id.'?language=fr';
            $balise_tmdb = '<a href='.$pageTMDB.'>Page TMDB</a>';
            
            $content= tmdbget("movie/".$id."/credits", ['language' => 'fr']);
            $json = json_decode($content);
            $cast = $json->{'cast'};
            $acteurs = array();
            $hobbits = "Hobbits: <br>";





            foreach($cast as $acteur) {
                if($acteur->{'known_for_department'} == "Acting")  {
                    if (isset($acteurs_film_compt[$acteur->{'name'}])) $acteurs_film_compt[$acteur->{'name'}] += 1;
                    else $acteurs_film_compt[$acteur->{'name'}] = 1;

                    if(strpos($acteur->{'character'},"Hobbit") !== false ||strpos($acteur->{'character'},"Bilbo") !== false||strpos($acteur->{'character'},"Frodo") !== false||strpos($acteur->{'character'},"Pippin") !== false||strpos($acteur->{'character'},"Merry") !== false||strpos($acteur->{'character'},"Sam") !== false) {
                        $hobbits .= $acteur->{'name'}.' ('.$acteur->{'character'}.')<br>';
                    }


                    array_push($acteurs, array($acteur->{'name'}, $acteur->{'character'}, $acteur->{'id'}));
                }
            }
            array_push($array, array($title,$overview,$poster_url,$balise_tmdb, $acteurs, $hobbits));
            
        }

        for($i = 0; $i < count($array[0]) ; $i++){
            echo '<tr>';
            foreach ($array as $film)
            {
                if($i == 4) {
                    echo '<td>';
                    echo '<div class="acteurs">Acteurs : </div>';
                    echo '<div class="acteurs_list">';
                    foreach($film[$i] as $acteur) {
 
                        echo '<a href="https://www.themoviedb.org/person/'.$acteur[2].'" >'.$acteur[0].'</a> ('.$acteur[1] .') ('.$acteurs_film_compt[$acteur[0]].' film(s))</br>'; 
                    }
                    echo '</div>';
                }
                else {
                    echo '<td>'.$film[$i].'</td>';
                }
            }
            echo '</tr>';
        }

        echo '</table>'; 

    ?>
</body>
</html>