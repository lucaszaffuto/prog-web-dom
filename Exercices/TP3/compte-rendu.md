% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : The Movie Database

## Participants 

* Thomas FOURNIER
* Joshua MONTEILLER
* Lucas ZAFFUTO

###Compte rendu

1) Le format de réponse est en Json. Il s’agit du film Fight Club et le paramètre language=fr permet de mettre les informations en français plutôt qu’en anglais.

2) voir le fichier themoviedatabase_cli.php

3-5) voir le fichier themoviedatabase.php

6-7) voir le fichier lotr.php

8) On peut afficher tous les acteurs qui jouent des Hobbits. Pour cela on regarde si le champ “character” contient le mot Hobbit. Il y a cependant des cas particuliers pour les Hobbits principaux où le champ “character” ne contient pas le mot Hobbit. On a donc rentré certains noms de Hobbit dans notre condition pour qu’il s’affiche aussi dans la liste des Hobbits.

9) Pour ce faire, il suffit d’entourer tous les noms des acteurs de balises <a href=”https://www.themoviedb.org/person/id”> en utilisant le champ id présent dans les données renvoyées par l’api pour chaque acteur. 

10) Pour réussir à intégrer un lecteur vidéo avec le trailer du film, nous avons ajouter la commande au fichier themoviedatabase_cli.php (avec la variable $trailer_youtube qui contient le lien vers une vidéo youtube du trailer) :
<embed type='video/webm' src='".$trailer_youtube."'width='600'height='400'>



