<!DOCTYPE html>
<html>
<head>
	<title>The Movie Database</title>
    <link href="tmdb.css" rel="stylesheet">
</head>
<body>
    <h1>The Movie Database</h1>
    <form method="get" action="">
        <label for="id">Id</label> <input type="id" id="id" <?php if(isset($_GET["id"])) {echo 'value="'.$_GET["id"].'"';}?>name="id" />
        <input type="submit" value="Submit"/>
    </form>
    <hr>
    <?php
    require_once("tp3-helpers.php");

    if(isset($_GET['id']))
    {
        echo '<table>';
        $id = $_GET['id'];
        $content_fr = tmdbget("movie/".$id, ['language' => 'fr']);
        $content_en = tmdbget("movie/".$id, ['language' => 'en']);


        $json_fr = json_decode($content_fr);
        $json_en = json_decode($content_en);
        $original_language = $json_en->{'original_language'};
        $content_vo = tmdbget("movie/".$id, ['language' => $original_language]);
        $json_vo = json_decode($content_vo);

        $yt_url = "https://www.youtube.com/embed/";
        $video_fr = tmdbget("movie/".$id."/videos", ['language' => 'fr']);
        $video_en = tmdbget("movie/".$id."/videos", ['language' => 'en']);
        $video_vo = tmdbget("movie/".$id."/videos", ['language' => $original_language]);

        $video_json_fr = json_decode($video_fr);
        $video_json_en = json_decode($video_en);
        $video_json_vo = json_decode($video_vo);

        $trailer_key_fr = $yt_url.$video_json_fr->{'results'}[0]->{'key'};
        $trailer_key_en = $yt_url.$video_json_en->{'results'}[0]->{'key'};
        $trailer_key_vo = $yt_url.$video_json_vo->{'results'}[0]->{'key'};
        
        $title_fr = $json_fr->{'title'};
        $overview_fr = $json_fr->{'overview'};
        $tagline_fr = $json_fr->{'tagline'};
        $poster_path_fr = $json_fr->{'poster_path'};
        $poster_url_fr = 'https://image.tmdb.org/t/p/w300'.$poster_path_fr;
        $pageTMDB_fr = 'https://www.themoviedb.org/movie/'.$id.'?language=fr';
        
        $title_en = $json_en->{'title'};
        $overview_en = $json_en->{'overview'};
        $tagline_en = $json_en->{'tagline'};
        $poster_path_en = $json_en->{'poster_path'};
        $poster_url_en = 'https://image.tmdb.org/t/p/w300'.$poster_path_en;
        $pageTMDB_en = 'https://www.themoviedb.org/movie/'.$id.'?language=en';

        $title_vo = $json_vo->{'title'};
        $overview_vo = $json_vo->{'overview'};
        $tagline_vo = $json_vo->{'tagline'};
        $poster_path_vo = $json_vo->{'poster_path'};
        $poster_url_vo = 'https://image.tmdb.org/t/p/w300'.$poster_path_vo;
        $pageTMDB_vo = 'https://www.themoviedb.org/movie/'.$id.'?language='.$original_language;


        
        echo '<tr>';
        echo '<td><img src='.$poster_url_vo.'></td>';
        echo '<td><img src='.$poster_url_en.'></td>';
        echo '<td><img src='.$poster_url_fr.'></td>';

        echo '</tr>';

        echo '<tr>';
        echo '<td class="title">'.$title_vo.'</td>';
        echo '<td class="title">'.$title_en.'</td>';
        echo '<td class="title">'.$title_fr.'</td>';
        echo '</tr>';

        echo '<tr>';
        echo '<td>'.$tagline_vo.'</td>';
        echo '<td>'.$tagline_en.'</td>';
        echo '<td>'.$tagline_fr.'</td>';
        echo '</tr>';
        
        echo '<tr>';
        echo '<td><p>'.$overview_vo.'</p></td>';
        echo '<td><p>'.$overview_en.'</p></td>';
        echo '<td><p>'.$overview_fr.'</p></td>';
        echo '</tr>';

        echo '<tr>';
        echo "<td><a href='".$pageTMDB_vo."'>Page en version originale</td>";
        echo "<td><a href='".$pageTMDB_en."'>Page en version anglaise</td>";
        echo "<td><a href='".$pageTMDB_fr."'>Page en version française</td>";
        echo '</tr>';

        echo '<tr>';
        echo "<td><embed type='video/webm' src='".$trailer_key_vo."'width='600'height='400'></td>";
        echo "<td><embed type='video/webm' src='".$trailer_key_en."'width='600'height='400'></td>";
        echo "<td><embed type='video/webm' src='".$trailer_key_fr."'width='600'height='400'></td>";
        echo '</tr>';

        echo '</table>';

        
        

    }

    ?>
</body>
</html>