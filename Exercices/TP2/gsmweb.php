<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Proximite bornes wifi</title>
</head>

<body>
    <h1>Proximite bornes wifi</h1>
    <form method="get" action="">
        <label for="lon">Lon</label> <input type="lon" id="lon" name="lon" required /><br />
        <label for="lat">Lat</label> <input type="lat" id="lat" name="lat" required /><br />
        <label for="top">Top</label> <input type="top" id="top" name="top" required /><br />
        <label for="ope-select">Operateur</label>
            <select name="ope" id="ope">
                <option value="tous">Tous les opérateurs</option>
                <option value="BYG">Opérateur Bouygues</option>
                <option value="ORA">Opérateur Orange</option>
                <option value="FREE">Opérateur Free</option>
                <option value="SFR">Opérateur SFR</option>
            </select>

        <input type="submit" value="Envoyer" />
    </form>
    <?php

    if (isset($_GET['top']) and isset($_GET['lat']) and isset($_GET['lon']) and isset($_GET['ope'])) {
        $top = $_GET['top'];
        $lat = $_GET['lat'];
        $lon = $_GET['lon'];
        $ope = $_GET['ope'];


        require_once("tp2-helpers.php");
        
        $array = array();
        $tm = array();



        $json = json_decode(file_get_contents("DSPE_ANT_GSM_EPSG4326.json"))->{'features'};
        for ($g = 0; $g < count($json); $g++) {
            $lon2 = $json[$g]->{'geometry'}->{'coordinates'}[0];
            $lat2 = $json[$g]->{'geometry'}->{'coordinates'}[1];
            $oper = $json[$g]->{'properties'}->{'OPERATEUR'};
            $adress = $json[$g]->{'properties'}->{'ANT_ADRES_LIBEL'};
            $DG = $json[$g]->{'properties'}->{'ANT_2G'};
            $TG = $json[$g]->{'properties'}->{'ANT_3G'};
            $QG = $json[$g]->{'properties'}->{'ANT_4G'};
            $tm = array_combine(array('adress', 'lon', 'lat', 'ope', '2g', '3g', '4g'), array($adress, $lon2, $lat2,$oper,$DG,$TG,$QG));
            if($ope == $oper or $ope == "tous"){
                array_push($array, $tm);
            }
        }
        
        $top5 = array();
        $p = geopoint($lon, $lat);
        $array_dist = array();
        $array_name = array();

        foreach ($array as $value) {
            $dist = distance($p, $value);
            
            array_push($array_dist, $dist);

            
        }
        echo "<br>Les " . $top . " points d'accès de l'operateur " .$ope. " les plus proches de vous sont :<br>";

        array_multisort($array_dist, $array);

        if ($top > count($array)) {
            $top = count($array);
        }
        echo "<table>";
        echo "<tr><th>Num</th><th>Point d'accès</th><th>Opérateur</th><th>Adresse</th><th>Distance</th><th>2G</th><th>3G</th><th>4G</th></tr>";
        
        for ($j = 0; $j < $top; $j++) {
            $a = $j + 1;
            $adresse = json_decode(smartcurl("https://api-adresse.data.gouv.fr/reverse/?lon=" . $array[$j]['lon'] . "&lat=" . $array[$j]['lat'], 0))->{'features'}[0]->{"properties"}->{"label"};

            echo "<tr><td>" . $a . "</td> <td> " . $array[$j]['adress'] . "</td> <td> " .$array[$j]['ope'] ."</td><td> " .$adresse ."</td><td> " .$array_dist[$j] ."m</td><td> " .$array[$j]['2g'] ."</td> <td> ". $array[$j]['3g']. "</td> <td> " . $array[$j]['4g'] . "</td></tr>";
        }
        echo "</table>";
    }
    ?>
</body>

</html>