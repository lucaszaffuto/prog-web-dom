<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Proximite bornes wifi</title>
</head>

<body>
    <h1>Proximite bornes wifi</h1>
    <form method="get" action="">
        <label for="lon">Lon</label> <input type="lon" id="lon" name="lon" required /><br />
        <label for="lat">Lat</label> <input type="lat" id="lat" name="lat" required /><br />
        <label for="top">Top</label> <input type="top" id="top" name="top" required /><br />

        <input type="submit" value="Envoyer" />
    </form>
    <?php

    if (isset($_GET['top']) and isset($_GET['lat']) and isset($_GET['lon'])) {
        $top = $_GET['top'];
        $lat = $_GET['lat'];
        $lon = $_GET['lon'];


        require_once("tp2-helpers.php");
        
        $array = array();
        $tm = array();



        $json = json_decode(file_get_contents("borneswifi_EPSG4326.json"))->{'features'};
        for ($g = 0; $g < count($json); $g++) {
            $lon2 = $json[$g]->{'properties'}->{'longitude'};
            $lat2 = $json[$g]->{'properties'}->{'latitude'};
            $name = $json[$g]->{'properties'}->{'AP_ANTENNE1'};
            $adress = $json[$g]->{'properties'}->{'AP_ANTENNE1'};
            $tm = array_combine(array('name', 'adress', 'lon', 'lat'), array($name, $adress, $lon2, $lat2));

            array_push($array, $tm);
        }
        $moins200 = array();
        $top5 = array();
        $p = geopoint($lon, $lat);
        $array_dist = array();
        $array_name = array();

        foreach ($array as $value) {
            $dist = distance($p, $value);
           // echo "Point d'accès : " . $value['name'] . ", distance : " . $dist . "m<br>";
            array_push($array_dist, $dist);

            if ($dist < 200) {
                array_push($moins200, array($value['name'], $dist));
            }
        }
        echo "<br>Les points d'accès de moins de 200 mètres sont : <br>";
        echo "<table>";
        echo "<tr><th>Point d'accès</th><th>Distance</th></tr>";
        foreach ($moins200 as $valeur) {
            echo "<tr><td>" . $valeur[0] . "</td> <td> " . $valeur[1] . " m</td> </tr>";
        }
        echo "</table>";
        echo "<br>Les " . $top . " points d'accès les plus proches de vous sont :<br>";

        array_multisort($array_dist, $array);

        if ($top > count($array)) {
            $top = count($array);
        }
        echo "<table>";
        echo "<tr><th>Num</th><th>Point d'accès</th><th>Distance</th><th>Adresse</th></tr>";
        
        for ($j = 0; $j < $top; $j++) {
            $a = $j + 1;
            $adresse = json_decode(smartcurl("https://api-adresse.data.gouv.fr/reverse/?lon=" . $array[$j]['lon'] . "&lat=" . $array[$j]['lat'], 0))->{'features'}[0]->{"properties"}->{"label"};

            echo "<tr><td>" . $a . "</td> <td> " . $array[$j]['name'] . "</td> <td> " . $array_dist[$j] . "m</td> <td> " . $adresse . "</tr>";
        }
        echo "</table>";
    }
    ?>
</body>

</html>