
<?php
    require_once ("tp2-helpers.php");
    $file_data = file($argv[1]);
    $ressource = fopen($argv[1],'r');
    $array = array();
    
    

    while(!feof($ressource)) array_push($array, fgetcsv($ressource,','));

    for($g = 0; $g<count($array); $g++){ 
        $array[$g] = array_combine(array('name','adress','lon','lat'),$array[$g]);
    }
    
    $moins200 = array();
    $top5 = array();
    $p = geopoint(5.72752,45.19102);
    $array_dist = array();
    $array_name = array();

    foreach ($array as $value) {
        $dist = distance($p,$value);
        echo "Point d'accès : ".$value['name'].", distance : ".$dist."m\n";
        array_push($array_dist,$dist);

        if($dist < 200) {
            array_push($moins200,array($value['name'],$dist));
        }
    }
    echo "\nLes points d'accès de moins de 200 mètres sont : \n";
    foreach($moins200 as $valeur){
        echo "Point d'accès ".$valeur[0]." à ".$valeur[1]." mètres\n";
    }

    echo "\nLes 5 points d'accès les plus proches de vous sont :\n";
    array_multisort($array_dist, $array);
    
    
    for($j=0;$j<5;$j++){
        $a = $j+1;
        $adresse = json_decode(smartcurl("https://api-adresse.data.gouv.fr/reverse/?lon=".$array[$j]['lon']."&lat=".$array[$j]['lat'], 0))->{'features'}[0]->{"properties"}->{"label"};

        echo "N°".$a." : ".$array[$j]['name']." : ".$array_dist[$j]."m. Adresse : ".$adresse."\n";
    }
    
?>


